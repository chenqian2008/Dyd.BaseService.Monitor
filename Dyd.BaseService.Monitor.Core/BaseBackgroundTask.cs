﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dyd.BaseService.Monitor.Core
{
    /// <summary>
    /// 监控任务后台数据采集的基类，子类只需设置一个间隔时间，写好处理方法，基类会维护好执行
    /// 简化开发，其内部实现为一个后台线程，每休眠间隔时间后执行一次数
    /// </summary>
    public abstract class BaseBackgroundTask
    {
        private System.Threading.Thread _thread = null;
        public int TimeSleep = 5000;
        public BaseBackgroundTask()
        {
           
        }

        public virtual void Start()
        {
            _thread = new System.Threading.Thread(TryRun);
            _thread.IsBackground = true;
            _thread.Start();
        }

        protected void TryRun()
        {
            while (true)
            {
                try
                {
                    Run();
                }
                catch (Exception exp)
                {
                    LogHelper.Error("当前后台任务出现严重错误:", exp);
                }
                System.Threading.Thread.Sleep(TimeSleep);
            }
        }

        //子类要实现的方法
        protected virtual void Run()
        {

        }
    }
}
